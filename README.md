API Rest escrita em Python para controlar um VFO utilizando o modulo Adafruit si5351 com um Raspberry PI B+.

Utiliza como base para comunicação com o si5351 o código disponível aqui: https://github.com/roseengineering/RasPi-Si5351.py.git