from flask import Flask, g
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from si5351 import Si5351
import RPi.GPIO as gpio
import math

# Utiliza a biblioteca Flask para criar a API Rest.
app = Flask(__name__)
CORS(app)

api = Api(app)

# Cria o objeto para usar o si5351
si = Si5351()

# Configura os pinos de GPIO que serao utilizados para detectar
# a posicao da chave de banda do radio.
#
# Isso e opcional para o RTH-220 e pode ser diferente em outros modelos de radio.
gpio.setmode(gpio.BCM)
gpio.setup(16, gpio.IN)
gpio.setup(20, gpio.IN)
gpio.setup(21, gpio.IN)

# Define as variaveis globais e inicializa com os valores padrao.
freq = 7000000
banda = '40m'
modo = 'LSB'

# Le a posicao da chave de banda e atualiza os valores.
if gpio.input(16) == 0:
    freq = 14000000
    banda = '20m'
    modo = 'USB'
if gpio.input(20) == 0:
    freq = 7000000
    banda = '40m'
    modo = 'LSB'
if gpio.input(21) == 0:
    freq = 3500000
    banda = '80m'
    modo = 'LSB'

@app.route("/")
def index():
    return "API para controle do transceptor Telefunken RTH-220"

# Verifica os limites de frequencia para cada banda.
def verificaLimites(f, b):
    if b == '15m':
	if (f >= 21000000) and (f <= 21450000):
	    return True
    if b == '20m':
	if (f >= 14000000) and (f <= 14350000):
	    return True
    if b == '40m':
	if (f >= 7000000) and (f <= 7300000):
	    return True
    if b == '80m':
	if (f >= 3500000) and (f <= 5000000):
	    return True
    return False

# Calculo da frequencia do vfo para o modo LSB.
#
# Segundo o manual de servico do RTH-220, para operacao em LSB o vfo tem que
# estar 1650KHz abaixo da frequencia desejada.
#
# O si5351 e configurado para uma referencia de 600MHz e
# depois utilizamos um divisor para chegar a frequecia desejada para o vfo.
#
# O divisor e determinado por (600MHz / vfo) e entao processado para o padrao:
# a + ( b / c )
#
# sendo:
# a = parte inteira.
# ( b / c ) = parte decimal em forma de fracao.
#
# ex: vfo = 7MHz
#     600000000 / 7000000 = 85,714285
#     a = 85
#     b = 714285
#     c = 1000000 (fixo para decimal com 6 digitos, limite do si5351)
#
def calculaLSB():
    global freq
    vfo = freq - 1650000 + 100
    divisor = 600000000.0 / vfo
    decimal = divisor % 1
    inteiro = divisor - decimal
    A = int(inteiro)
    decimal = round(decimal, 6)
    decimal = decimal * 1000000
    B = int(decimal)
    ativa(A,B)

# Para o modo USB o vfo e colocado 1650KHz acima da frequencia desejada.
#
# Mesmo calculo do modo anterior
#
def calculaUSB():
    global freq
    vfo = freq + 1650000 - 100
    divisor = 600000000.0 / vfo
    decimal = divisor % 1
    inteiro = divisor - decimal
    A = int(inteiro)
    decimal = round(decimal, 6)
    decimal = decimal * 1000000
    B = int(decimal)
    ativa(A,B)

# Configura o si5351 com os valores calculados para o modo e frequencia escolhidos
def ativa(a,b):
    si.setupPLL(si.PLL_A, 24) # Multiplica os 25MHz do cristal interno para gerar os 600MHz
    si.setupMultisynth(0, si.PLL_A, int(a), int(b), 1000000) # Define a saida CLK0 e o divisor calculado
    si.enableOutputs(True) # Habilita o vfo

# Rotas da API Rest:
#
class DefineFrequencia(Resource):
    def post(self):
	parser = reqparse.RequestParser()
	parser.add_argument('freq', required=True)
	parser.add_argument('banda', required=True)
	args = parser.parse_args()
	global freq, banda, modo
	freq = int(args['freq'])
	banda = args['banda']
	if verificaLimites(freq, banda):
	    if modo == 'USB':
		calculaUSB()
	    else:
		calculaLSB()
	    return {'frequencia': freq, 'modo':modo}, 200
	return {'message': 'Erro: Fora de Faixa...'}, 200

class AumentaFrequencia(Resource):
    def post(self):
	parser = reqparse.RequestParser()
	parser.add_argument('step', required=True)
	args = parser.parse_args()
	step = int(args['step'])
	global freq, banda, modo
	freq = freq + step
	if verificaLimites(freq, banda):
	    if modo == 'USB':
		calculaUSB()
	    else:
		calculaLSB()
	    return {'frequencia': freq, 'modo': modo}, 200
	freq = freq - step
	return {'message': 'Erro: Fora de Faixa...'}, 200

class DiminuiFrequencia(Resource):
    def post(self):
	parser = reqparse.RequestParser()
	parser.add_argument('step', required=True)
	args = parser.parse_args()
	step = int(args['step'])
	global freq, banda, modo
	freq = freq - step
	if verificaLimites(freq, banda):
	    if modo == 'USB':
		calculaUSB()
	    else:
		calculaLSB()
	    return {'frequencia': freq, 'modo': modo}, 200
	freq = freq + step
	return {'message': 'Erro: Fora de Faixa...'}, 200

class DefineModo(Resource):
    def post(self):
	parser = reqparse.RequestParser()
	parser.add_argument('modo', required=True)
	args = parser.parse_args()
	global freq, banda, modo
	modo = args['modo']
	if modo == 'USB':
	    calculaUSB()
	else:
	    calculaLSB()
	return {'frequencia': int(freq), 'banda': banda, 'modo': modo}, 200

class MostraFrequenciaAtual(Resource):
    def post(self):
	global freq, banda, modo
	return {'frequencia': int(freq), 'banda': banda, 'modo': modo}, 200

class IdentificaBanda(Resource):
    def post(self):
	if gpio.input(16) == 0:
	    return {'banda': '20m'}, 200
	if gpio.input(20) == 0:
	    return {'banda': '40m'}, 200
	if gpio.input(21) == 0:
	    return {'banda': '80m'}, 200
	return {'banda': 'erro'}, 200

api.add_resource(DefineFrequencia, '/defineFrequencia')
api.add_resource(AumentaFrequencia, '/aumentaFrequencia')
api.add_resource(DiminuiFrequencia, '/diminuiFrequencia')
api.add_resource(DefineModo, '/defineModo')
api.add_resource(MostraFrequenciaAtual, '/mostraFrequenciaAtual')
api.add_resource(IdentificaBanda, '/identificaBanda')
